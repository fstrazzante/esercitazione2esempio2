package it.unibg.so.lab2;

public class Message {
	
	private String message = null;
	
	public synchronized void setMessage(String m){
		message = m;
		System.out.println("notify...");
		notify();
	}
	
	public synchronized String getMessage(){
		if(message == null){
			System.out.println("wait...");
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return message;
	}
	
	public static void main(String[] args){
		Message m = new Message();
		new ReceiverThread(m).start();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		m.setMessage("Hello.");
	}

}

class ReceiverThread extends Thread{
	
	private Message m;
	
	public ReceiverThread(Message m){
		this.m = m;
	}

	@Override
	public void run() {
		System.out.println("Message received: " + m.getMessage());
	}
	
}
